﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerMonitor.Selector
{
    public class Selector
    {
        public IEnumerable<ping.propertiesping> ping { get; set; }
        public IEnumerable<Website.Propertiessites> WebPage { get; set; }
        public IEnumerable<DBConnection.propertiesDB>DatabaseDetails { get; set; }
        
    }
}
