﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServerMonitor.Website
{
    public class SiteStatus
    {
        public void Site_Status(Servers.Servers _objServer)
        {
            int count = 1;
            foreach (var item in _objServer.selector.WebPage)
            {
                try
                {
                    Console.WriteLine(count + ") SiteName:" + item.SiteName);
                    var url = item.Link;

                    WebRequest req = WebRequest.Create(url);

                    WebResponse res = req.GetResponse();

                    item.Status = " Working Fine Now" ;
                    item.DateTime = "(" + DateTime.Now + ")";
                    Console.WriteLine("Link:" + url + "" + item.Status+""+item.DateTime);
                }
                catch (WebException ex)
                {
                    item.Status = " Not Working Fine Now";
                    item.DateTime = "(" + DateTime.Now + ")";
                    Console.WriteLine("Link:" + item.Link + "" + item.Status+""+item.DateTime);

                }
                count++;
            }
          
        }
    }
}
