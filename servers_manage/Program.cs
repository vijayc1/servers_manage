﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.NetworkInformation;
using System.Net.Mail;
using System.Collections;

namespace ServerMonitor
{
    public class Program
    {

        static void Main(string[] args)
        {
            string result = null;
            string filepath = ConfigurationSettings.AppSettings["Path"].ToString();

            StreamReader reader = new StreamReader(filepath);
            result = reader.ReadToEnd();
            Servers.Servers _objServer = JsonConvert.DeserializeObject<Servers.Servers>(result);
            Console.WriteLine(_objServer);
            ping.ping ping = new ping.ping();
            Website.SiteStatus site = new Website.SiteStatus();
            DBConnection.DbConnection connection = new DBConnection.DbConnection();
            ping.ping_status(_objServer);            
            site.Site_Status(_objServer);       
            connection.DBConnection(_objServer);
            Mail.mail mail = new Mail.mail();
            mail.email_send(_objServer);

            Console.ReadLine();  
        }
        
    }
}
