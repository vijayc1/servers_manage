﻿using MySql.Data.MySqlClient;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerMonitor.DBConnection
{
    
    public class DbConnection 
    {       
        public void DBConnection(Servers.Servers _objServer)
        {
            
            IDbConnection connection;
            DataTable dt = new DataTable();
            foreach (var con in _objServer.selector.DatabaseDetails)
            {
                
                if (con.DBType == "SQL")
                {
                    connection = new System.Data.SqlClient.SqlConnection("Data Source=" + con.DBHost + ";Initial Catalog=" + con.DBDatabase + ";User ID =" + con.DBUserName + "; Password =" + con.DBPassWord);
                    connection.Open();
                    
                    foreach (var a in con.Queries)
                    {
                        try
                        {
                            a.status = new List<string>();
                            string Connectionstring = "Data Source=" + con.DBHost + ";Initial Catalog=" + con.DBDatabase + ";User ID =" + con.DBUserName + "; Password =" + con.DBPassWord;
                            IDbCommand command = connection.CreateCommand();
                            command.CommandText = "" + a.Query;
                            Console.WriteLine(a.Title);
                            SqlDataAdapter da = new SqlDataAdapter(command.CommandText, Connectionstring);
                            DataSet ds = new DataSet();
                            da.Fill(ds);
                            dt = ds.Tables[0];
                            List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();
                            foreach (var ta in rows)
                            {
                                List<string> list = new List<string>();

                                foreach (var r in ta.ItemArray)
                                {
                                    Console.WriteLine(r.ToString());
                                }
                            }
                            DBConnection.TableCreate create = new DBConnection.TableCreate();
                            var z = create.Table_Create(rows);
                            
                            foreach (var t in z)
                            {
                                a.status.Add(t);
                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("" + a.Title + "contains some problem so doesn't show");
                            a.status.Add("" + a.Title + "Table contains some problem so doesn't show");
                        }
                        connection.Close();
                    }                   
                }
                else if (con.DBType == "MYSQL")
                {
                    connection = new MySql.Data.MySqlClient.MySqlConnection("server=" + con.DBHost + ";database=" + con.DBDatabase + ";user=" + con.DBUserName + "; password =" + con.DBPassWord);
                    connection.Open();

                    foreach (var a in con.Queries)
                    {
                        try
                        {
                            a.status = new List<string>();
                            string Connectionstring = "server=" + con.DBHost + ";database=" + con.DBDatabase + ";user=" + con.DBUserName + "; password =" + con.DBPassWord;
                            IDbCommand command = connection.CreateCommand();
                            command.CommandText = "" + a.Query;
                            Console.WriteLine(a.Title);                            
                            MySqlDataAdapter da = new MySqlDataAdapter(command.CommandText, Connectionstring);                           
                            DataSet ds = new DataSet();                           
                            da.Fill(ds);                           
                            dt = ds.Tables[0];
                            List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();
                            foreach (var ta in rows)
                            {
                                List<string> list = new List<string>();
                                foreach (var r in ta.ItemArray)
                                {
                                    Console.WriteLine(r.ToString());                                    
                                }
                            }
                            DBConnection.TableCreate create = new DBConnection.TableCreate();
                            var z = create.Table_Create(rows);
                            
                            foreach (var t in z)
                            {
                                a.status.Add(t);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("" + a.Title + "contains some problem so doesn't show");
                            a.status.Add("" + a.Title + "Table contains some problem so doesn't show");
                        }
                    }
                    connection.Close();
                }
                else 
                {
                    connection = new Npgsql.NpgsqlConnection("Server=" + con.DBHost + ";Port=" + con.DBPort + ";Database=" + con.DBDatabase + ";User Id=" + con.DBUserName + "; Password=" + con.DBPassWord);
                    connection.Open();
                    foreach (var a in con.Queries)
                    {
                        try
                        {
                            a.status = new List<string>();
                            string Connectionstring = "Server=" + con.DBHost + ";Port=" + con.DBPort + ";Database=" + con.DBDatabase + ";User Id=" + con.DBUserName + "; Password=" + con.DBPassWord;
                            IDbCommand command = connection.CreateCommand();
                            command.CommandText = "" + a.Query;
                            NpgsqlDataAdapter da = new NpgsqlDataAdapter(command.CommandText, Connectionstring);
                            DataSet ds = new DataSet();
                            da.Fill(ds);
                            dt = ds.Tables[0];
                            List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();
                            foreach (var ta in rows)
                            {
                                List<string> list = new List<string>();
                                foreach (var r in ta.ItemArray)
                                {
                                    Console.WriteLine(r.ToString());
                                }
                            }
                            DBConnection.TableCreate create = new DBConnection.TableCreate();
                            var z = create.Table_Create(rows);
                           
                            foreach (var t in z)
                            {
                                a.status.Add(t);
                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("" + a.Title + "contains some problem so doesn't show");
                            a.status.Add("" + a.Title + "Table contains some problem so doesn't show");
                        }
                        connection.Close();
                    }                    
                }
            }
        }
    }
}
