﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerMonitor.DBConnection
{
    public class TableCreate
    {

        // change as html formatter
        List<string> list1= new List<string>();
        public IEnumerable<string> Table_Create(IEnumerable<DataRow> _objServer)
        {           
            var td = "<td>";
            var etd = "</td>";
            var con1 = "<tr>";
            var con2 = "</tr>";
            List<string> listofdatas = new List<string>();
            foreach (var a in _objServer)
            {
                var tosend = "";
                for (int i = 0; i < a.ItemArray.Count(); i++)
                {
                    tosend += td + a.ItemArray[i] + etd;
                }
                listofdatas.Add(con1 + tosend + con2);
            }
            var list = "";
            foreach (var li in listofdatas)
            {
                list += li;
            }
            var table = "<table border=1>" + list + "</table>";
            list1.Add(table);
            return list1;
        }
        
    }
}
