﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerMonitor.DBConnection
{
    public class propertiesDB
    {
        public string Title { get; set; }
        public string DBType { get; set; }
        public string DBHost { get; set; }
        public string DBPort { get; set; }
        public string DBDatabase { get; set; }
        public string DBUserName { get; set; }
        public string DBPassWord { get; set; }       
        public IEnumerable<propertiesQueries> Queries { get; set; }
       
    }
}
