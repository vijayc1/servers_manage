﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ServerMonitor.ping
{
    public class ping
    {
        public void ping_status(Servers.Servers _objServer)
        {
            int count = 1;
            foreach (var item in _objServer.selector.ping)
            {
                bool pingable = false;
                Ping pinger = null;
                if (item.IPAddress != null)
                {
                    try
                    {
                        pinger = new Ping();
                        PingReply reply = pinger.Send(item.IPAddress);
                        pingable = reply.Status == IPStatus.Success;
                        Console.WriteLine(count + ")Server Name:" + item.ServerName + " IPAddress:" + item.IPAddress);
                        Console.WriteLine("Status:" + pingable);
                        item.Status = pingable.ToString();

                    }
                    catch (PingException)
                    {
                        Console.WriteLine("false");
                    }
                    finally
                    {
                        if (pinger != null)
                        {
                            pinger.Dispose();
                        }
                    }

                }
                count++;
            }
            
        }
    }
}
